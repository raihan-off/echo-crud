package domain

import "crud_echo/pkg/dto"

type User struct {
	Id       int `gorm:"bigserial"`
	Username string
	Address  string
	Email    string
	Password string
}

type UserRepository interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req User) error
	UpdateUser(id int, req User) error
	DeleteUser(id int) error
}

type UserUsecase interface {
	GetUsers() ([]User, error)
	GetUser(id int) (User, error)
	CreateUser(req dto.UserDTO) error
	UpdateUser(id int, req dto.UserDTO) error
	DeleteUser(id int) error
}

type Register struct {
	Username        string `json:"name"`
	Email           string `json:"email"`
	Address         string `json:"address"`
	Password        string `json:"password"`
	PasswordConfirm string `json:"password_confirm"`
}

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
