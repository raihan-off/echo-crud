package middleware

import (
	"context"
	"crud_echo/shared/authresponse"
	"crud_echo/shared/thelp"
	"net/http"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get("Authorization")

		if accessToken == "" {
			authresponse.Response(w, 401, "unauthorized", nil)
			return
		}

		user, err := thelp.ValidateToken(accessToken)
		if err != nil {
			authresponse.Response(w, 401, err.Error(), nil)
			return
		}

		ctx := context.WithValue(r.Context(), "userinfo", user)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
