package repository

import (
	"crud_echo/pkg/domain"
	"database/sql"
)

type UserRepository struct {
	db *sql.DB //nil
}

// GetUser implements domain.UserRepository.
func (*UserRepository) GetUser(id int) (domain.User, error) {
	panic("unimplemented")
}

func NewUserRepository(db *sql.DB) domain.UserRepository {
	return &UserRepository{
		db: db,
	}
}

func (ur UserRepository) GetUsers() ([]domain.User, error) {
	sql := `SELECT * FROM user`
	rows, err := ur.db.Query(sql)
	var users []domain.User
	for rows.Next() {
		var user domain.User
		err2 := rows.Scan(&user.Id, &user.Username, &user.Address, &user.Email, &user.Password)
		if err2 != nil {
			return nil, err2
		}
		users = append(users, user)
	}
	return users, err
}

func (ur UserRepository) GetStudent(id int) (domain.User, error) {
	var user domain.User
	sql := `SELECT * FROM student WHERE id = $1`

	err := ur.db.QueryRow(sql, id).Scan(&user.Id, &user.Username, &user.Address, &user.Password)
	return user, err
}

func (ur UserRepository) CreateUser(req domain.User) error {
	sql := `INSERT INTO user (username, address, email, password) values ($1, $2, $3, $4)`
	stmt, err := ur.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()
	_, err2 := stmt.Exec(req.Username, req.Address, req.Email, req.Password)
	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) UpdateUser(id int, req domain.User) error {
	sql := `
	UPDATE user 
	SET username = $2, address = $3, email = $4, password = $5 
	WHERE id = $1
	`
	stmt, err := ur.db.Prepare(sql)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err2 := stmt.Exec(id, req.Username, req.Address, req.Email, req.Password)
	if err2 != nil {
		return err2
	}
	return nil
}

func (ur UserRepository) DeleteUser(id int) error {
	sql := `DELETE FROM user WHERE id = $1`
	_, err := ur.db.Exec(sql, id)
	if err != nil {
		return err
	}
	return nil
}
