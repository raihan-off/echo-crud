package dto

import (
	"errors"

	validation "github.com/go-ozzo/ozzo-validation"
)

type UserDTO struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Address  string `json:"address"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u UserDTO) Validation() error {
	if u.Id == 0 {
		return errors.New("ID is required for update")
	}

	err := validation.ValidateStruct(&u,
		validation.Field(&u.Username, validation.Required),
		validation.Field(&u.Address, validation.Required),
		validation.Field(&u.Email, validation.Required),
		validation.Field(&u.Password, validation.Required))
	if err != nil {
		return err
	}
	return nil
}
