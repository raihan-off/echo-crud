package controller

import (
	"crud_echo/config"
	"crud_echo/pkg/domain"
	"crud_echo/shared/authresponse"
	"crud_echo/shared/password"
	"crud_echo/shared/thelp"
	"encoding/json"
	"net/http"
)

func Register(w http.ResponseWriter, r *http.Request) {
	var register domain.Register

	if err := json.NewDecoder(r.Body).Decode(&register); err != nil {
		authresponse.Response(w, 500, err.Error(), nil)
		return
	}

	defer r.Body.Close()

	if register.Password != register.PasswordConfirm {
		authresponse.Response(w, 400, "Password not match", nil)
		return
	}

	passwordHash, err := password.HashPassword(register.Password)
	if err != nil {
		authresponse.Response(w, 500, err.Error(), nil)
		return
	}

	user := domain.User{
		Username: register.Username,
		Email:    register.Email,
		Password: passwordHash,
	}

	if err := config.DB.Create(&user).Error; err != nil {
		authresponse.Response(w, 500, err.Error(), nil)
		return
	}

	authresponse.Response(w, 201, "Register Successfully", nil)
}

func Login(w http.ResponseWriter, r *http.Request) {
	var login domain.Login

	if err := json.NewDecoder(r.Body).Decode(&login); err != nil {
		authresponse.Response(w, 500, err.Error(), nil)
		return
	}

	var user domain.User
	if err := config.DB.First(&user, "email = ?", login.Email).Error; err != nil {
		authresponse.Response(w, 404, "Wrong email or password", nil)
		return
	}

	if err := password.VerifyPassword(user.Password, login.Password); err != nil {
		authresponse.Response(w, 404, "Wrong email or password", nil)
		return
	}

	token, err := thelp.CreateToken(&user)
	if err != nil {
		authresponse.Response(w, 500, err.Error(), nil)
		return
	}

	authresponse.Response(w, 200, "Successfully Login", token)
}
